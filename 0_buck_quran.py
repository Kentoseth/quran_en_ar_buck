import json
import pyaramorph.buckwalter as buck
import csv
import re

quran_list = []
quran_dict = {}

with open('corpus_clean.txt', newline = '') as corpus:

    corp_data = csv.reader(corpus, delimiter='\t')

    #print(type(corp_data))
    #<class '_csv.reader'>

    string_store = ""

    surah_check = 1

    verse_check = 1

    str_ref = "1:1"

    for row in corp_data:
        #print(row)
        #str

        parts = row[0].split(":")
        #print(parts)
        #<class 'list'>

        parts[0] = re.sub('[^0-9]','', parts[0])

        parts[3] = re.sub('[^0-9]','', parts[3])

        parts = [int(item) for item in parts]

        #print(parts)

        #print(verse_check)
        #prints correctly

        if parts[0] == surah_check:

            #print(surah_check)

            if parts[1] == verse_check:

                #print(row)

                string_store = string_store + " " + row[1]

                #print(string_store)
                #only verse 2:1 is missing

            elif parts[1] > verse_check:

                #print(row)
                #prints all those missing rows from above

                quran_dict[str_ref] = string_store

                #print(quran_dict)

                verse_check = parts[1]

                #print(str_ref, quran_dict)

                #print(verse_check)

                str_ref = str(parts[0]) + ":" + str(parts[1])

                string_store = row[1]

                #print(str_ref,string_store)

            '''
            else:

                print("X")

                quran_dict[str_ref] = string_store

                print(quran_dict)
            '''
            #the else above has no effect now


        elif parts[0] > surah_check:

            #print("check works")
            #print(row)

            #print(quran_dict)

            quran_dict[str_ref] = string_store

            #print(quran_dict)

            quran_list.append(quran_dict)

            #print(quran_list)

            quran_dict = {}

            #print(quran_list)

            #print(row)

            surah_check = parts[0]

            verse_check = 1

            str_ref = str(parts[0]) + ":" + str(parts[1])

            string_store = row[1]

            #print(surah_check, verse_check, str_ref)

    #print(row)
    #['(2:4:12:2)', 'wna', 'PRON', 'SUFFIX|PRON:3MP']

    #print(string_store)
    # wa {l~a*iyna yu&ominu wna bi maA^ >unzila <ilayo ka wa maA^ >unzila min qaboli ka wa bi {lo 'aAxirapi humo yuwqinu wna

    #print(quran_list)

    quran_dict[str_ref] = string_store
    #print(quran_dict)
    quran_list.append(quran_dict)
    #print(quran_list)
    #print(string_store)

#print(quran_list)

with open("quran_buckwalter.json", "w+") as f:

    json.dump(quran_list, f)

    print("Done!")
