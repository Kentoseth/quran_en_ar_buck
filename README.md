# quran_en_ar_buck

Providing the entire Quran in English (Sahih International translation), Arabic (from the Buckwalter transliteration) and Buckwalter transliteration (from the [Quran Corpus](https://corpus.quran.com/))

This repo provides the transformed JSON data and the scripts for transforming the raw data into JSON.

The data in this repo will form the foundations for an improved [85% of the Quran with contextual examples](https://www.dragomen.org/projects/85-percent-quran/)

## Contents

 * [Structure](#structure)
 * [Requirements](#requirements)
 * [Installation](#installation)
 * [Usage](#usage)
 * [Contribute](#contribute)
 * [License](#license)
 * [Queries](#queries)

## Structure

The structure of this repo is as follows:

 - The root folder contains the Python scripts marked in order of execution by numbers: 0_ and 1_
 - The root folder contains the raw data `corpus.txt` sourced from [here](http://textminingthequran.com/data/quranic-corpus-morphology-0.4.txt) and available [here as well](https://corpus.quran.com/download/) and `english_sahih.txt` sourced from [here](https://github.com/islamic-network/api.alquran.cloud)
 - `data/ folder containing the JSON data
 - `data/quran_buckwalter.json` contains the verse number and buckwalter text for each aya/verse of the Quran
 - `data/quran_arabic_buckwalter_translation.json` contains the verse number that has the: Arabic text (from the Buckwalter transliteration), English translation (Sahih International) and Buckwalter transliteration

## Requirements

You will need the following:

 - system that supports Python 3
 - have a virtual environment (optional)

## Installation

It is recommended to use a virtual environment. The following instructions will assume that a virtual environment is being used. If not, replace the commands with their appropriate alternative (like `pip3` instead of `pip`).

Run this command:

`pip install pyaramorph`

Clone the repo by running:

`git clone git@codeberg.org:Kentoseth/quran_en_ar_buck.git`

This repo is available at 4 locations for redundancy:

[Codeberg](https://codeberg.org/Kentoseth/quran_en_ar_buck)
[GitLab](https://gitlab.com/dragomen/quran_en_ar_buck)
[BitBucket](https://bitbucket.org/Kentoseth/quran_en_ar_buck/src/master/)
[OpenCode](https://www.opencode.net/dragomen/quran_en_ar_buck)

Then navigate into the root of the directory:

`cd quran_en_ar_buck/`

Open the file `corpus.txt` in a text editor and remove the first 57 lines so that the file starts with: `(1:1:1:1)	bi	P	PREFIX|bi+` and rename the modified file as `corpus_clean.txt`.

(The reason for doing the modification manually is to respect the copyright notice that does not permit distribution of a modified corpus text).

## Usage

Run the following 2 commands in order:

```
python 0_buck_quran.py
...
python 1_quran_arabic_trans.py
```

2 files will be created in the same directory:

`quran_buckwalter.json` and `quran_arabic_buckwalter_translation.json`

The latter file contains the valuable data that may be of benefit to others.

## Contribute

For any issues, the primary repo to open them is on Codeberg [here](https://codeberg.org/Kentoseth/quran_en_ar_buck/issues).

If you do not use Codeberg, you can reach me on [Mastodon as well](https://fosstodon.org/web/@kentoseth) (note that you can use any Mastodon instance to send me a Toot, as the service is federated).

## License

The code in this repo is licensed under GPLv3+.

The raw data is licensed under their respective licenses and any derivative data (the JSON) will continue to observe the respective licensing.

## Queries

For discussions about this project or [Dragomen](https://www.dragomen.org/) in general, you can reach out to me through my [website](https://www.kentoseth.com/) or [Mastodon](https://fosstodon.org/web/@kentoseth)
